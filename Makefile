DB_URL := "postgres://postgres:member1206@localhost:5432/food_delivery?sslmode=disable"

run:
	go run cmd/main.go

migrate-up:
	migrate -path migrations -database $(DB_URL) -verbose up

migrate-down:
	migrate -path migrations -database $(DB_URL) -verbose down

migrate-force:
	migrate -path migrations -database $(DB_URL) -verbose force 1

migrate-file:
	migrate create -ext sql -dir migrations/ -seq admin_tables

swag-gen:
	~/go/bin/swag init -g ./api/router.go -o api/docs
