package main

import (
	"fmt"
	"food-delivery/api"
	"food-delivery/config"
	"food-delivery/pkg/logger"
	"food-delivery/storage"
	"log"

	"github.com/jmoiron/sqlx"
)

func main() {
	cfg := config.Load()
	var psqlUrl = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase,
	)
	db, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatal("failed connecting database : ", err)
	}

	log := logger.New(cfg.LogLevel, "food-delivery")

	serviceManager := storage.NewStoragePg(db)

	server := api.New(api.Option{
		Conf:    cfg,
		Logger:  log,
		Storage: serviceManager,
	})

	if err := server.Run(fmt.Sprintf(":%s", cfg.HTTPPort)); err != nil {
		log.Fatal("failed to run http server", logger.Error(err))
		panic(err.Error())
	}
}
