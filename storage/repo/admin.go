package repo

import (
	"context"
	mdl "food-delivery/api/handlers/models"
)

type AdminStorageI interface {
	AdminLogin(ctx context.Context, req *mdl.AdminLoginReq) (*mdl.Admin, error)
	// GetAllUsers(ctx context.Context, req *mdl.GetAllUsersReq) (*mdl.GetAllUsersRes, error)
	// GetUserById(ctx context.Context, id string) (*mdl.User, error)
	// DeleteUser(ctx context.Context, req *mdl.DeleteUserReq) (*mdl.DeleteUserRes, error)
	Exists(ctx context.Context, req *mdl.GetInfoReq) (*mdl.Admin, error)
}
