package repo

import (
	"context"
	mdl "food-delivery/api/handlers/models"
)

type UserStorageI interface {
	Login(ctx context.Context, req *mdl.UserLoginReq) (*mdl.User, error)
	CreateUser(ctx context.Context, req *mdl.CreateUserReq) (*mdl.User, error)
	GetUser(ctx context.Context, req *mdl.GetUserReq) (*mdl.User, error)
	UpdateUser(ctx context.Context, req *mdl.UpdateUserReq) (*mdl.User, error)
	DeleteUser(ctx context.Context, req *mdl.DeleteUserReq) (*mdl.DeleteUserRes, error)
	Exists(ctx context.Context, req *mdl.GetInfoReq) (*mdl.User, error)
}
