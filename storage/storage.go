package storage

import (
	"food-delivery/storage/postgres"
	"food-delivery/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	User() repo.UserStorageI
	Admin() repo.AdminStorageI
}

type Pg struct {
	db        *sqlx.DB
	userRepo  repo.UserStorageI
	adminRepo repo.AdminStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) *Pg {
	return &Pg{
		db:        db,
		userRepo:  postgres.NewUserRepo(db),
		adminRepo: postgres.NewAdminRepo(db),
	}
}

func (s Pg) User() repo.UserStorageI {
	return s.userRepo
}

func (s Pg) Admin() repo.AdminStorageI {
	return s.adminRepo
}
