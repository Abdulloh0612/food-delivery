package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"food-delivery/api/handlers/models"

	"github.com/jmoiron/sqlx"
)

type AdminRepo struct {
	db *sqlx.DB
}

func NewAdminRepo(db *sqlx.DB) *AdminRepo {
	return &AdminRepo{db: db}
}

func (a *AdminRepo) AdminLogin(ctx context.Context, req *models.AdminLoginReq) (*models.Admin, error) {
	var res models.Admin
	var updatedAt sql.NullTime
	query := `
	SELECT id, user_name, created_at, updated_at FROM super_admins WHERE user_name = $1 AND password = $2
	`
	err := a.db.DB.QueryRow(query, req.UserName, req.Password).Scan(
		&res.Id,
		&res.UserName,
		&res.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	if updatedAt.Valid {
		res.UpdatedAt = updatedAt.Time.Format(time.RFC3339)
	}

	_, err = a.db.Exec(`UPDATE super_admins SET refresh_token = $1 WHERE id = $2`, res.RefreshToken, res.Id)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

// func (repo *AdminRepo) Create(ctx context.Context, req *models.Admin) (*models.Admin, error) {
// 	admin := &models.Admin{}
// 	query := "INSERT INTO super_admins (id, user_name, password, refresh_token) VALUES ($1, $2, $3, $4) RETURNING id, user_name, password, refresh_token, created_at, updated_at"
//
// 	err := repo.db.QueryRowContext(ctx, query, req.Id, req.UserName, req.Password, req.RefreshToken).Scan(
// 		&admin.Id, &admin.UserName, &admin.Password, &admin.RefreshToken, &admin.CreatedAt, &admin.UpdatedAt)
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return admin, nil
// }

func (repo *AdminRepo) Update(ctx context.Context, updatedAdmin *models.UpdateAdminReq) (*models.Admin, error) {
	admin := &models.Admin{}
	query := "UPDATE super_admins SET user_name = $1, password = $2, refresh_token = $3, updated_at = $4 WHERE id = $5 RETURNING id, user_name, password, refresh_token, created_at, updated_at"
	err := repo.db.QueryRowContext(ctx, query, updatedAdmin.UserName, updatedAdmin.Password, updatedAdmin.RefreshToken, time.Now(), updatedAdmin.Id).Scan(
		&admin.Id, &admin.UserName, &admin.Password, &admin.RefreshToken, &admin.CreatedAt, &admin.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return admin, nil
}

func (repo *AdminRepo) DeleteUser(ctx context.Context, id string) error {
	query := "UPDATE super_admins SET deleted_at = $1 WHERE id = $2"
	_, err := repo.db.ExecContext(ctx, query, time.Now().Unix(), id)
	if err != nil {
		return err
	}
	return nil
}

func (repo *AdminRepo) Exists(ctx context.Context, req *models.GetInfoReq) (*models.Admin, error) {
	res := &models.Admin{}
	var updatedAt sql.NullTime
	query := fmt.Sprintf("SELECT id, user_name, password, created_at, updated_at FROM super_admins WHERE %s = $1", req.Field)
	err := repo.db.QueryRowContext(ctx, query, req.Value).Scan(
		&res.Id,
		&res.UserName,
		&res.Password,
		&res.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	if updatedAt.Valid {
		res.UpdatedAt = updatedAt.Time.Format(time.RFC3339)
	}

	return res, nil
}
