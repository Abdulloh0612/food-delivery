package postgres

import (
	"context"
	"database/sql"
	"fmt"
	mdl "food-delivery/api/handlers/models"
	"time"

	"github.com/jmoiron/sqlx"
)

type UserRepo struct {
	db *sqlx.DB
}

func NewUserRepo(db *sqlx.DB) *UserRepo {
	return &UserRepo{db: db}
}

func (u *UserRepo) Login(ctx context.Context, req *mdl.UserLoginReq) (*mdl.User, error) {
	var res mdl.User
	var updatedAt sql.NullTime
	query := `
	SELECT id, first_name, last_name, phone_number, birth_date, created_at, updated_at FROM customers WHERE phone_number = $1
	`
	err := u.db.DB.QueryRow(query, req.PhoneNumber).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.PhoneNumber,
		&res.BirthDate,
		&res.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}
	if updatedAt.Valid {
		res.UpdatedAt = updatedAt.Time.Format(time.RFC3339)
	}

	_, err = u.db.Exec(`UPDATE customers SET refresh_token = $1 WHERE id = $2`, req.RefreshToken, res.Id)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (repo *UserRepo) CreateUser(ctx context.Context, req *mdl.CreateUserReq) (*mdl.User, error) {
	user := &mdl.User{}
	query := "INSERT INTO customers (id, first_name, last_name, phone_number, birth_date, refresh_token) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, first_name, last_name, phone_number, birth_date, refresh_token, created_at, updated_at"

	var updatedAt sql.NullTime

	err := repo.db.QueryRowContext(ctx, query, req.Id, req.FirstName, req.LastName, req.PhoneNumber, req.BirthDate, req.RefreshToken).Scan(
		&user.Id, &user.FirstName, &user.LastName, &user.PhoneNumber, &user.BirthDate, &user.RefreshToken, &user.CreatedAt, &updatedAt)
	if err != nil {
		return nil, err
	}

	if updatedAt.Valid {
		user.UpdatedAt = updatedAt.Time.Format(time.RFC3339)
	}

	return user, nil
}

func (u *UserRepo) GetUser(ctx context.Context, req *mdl.GetUserReq) (*mdl.User, error) {
	return &mdl.User{}, nil
}
func (u *UserRepo) UpdateUser(ctx context.Context, req *mdl.UpdateUserReq) (*mdl.User, error) {
	return &mdl.User{}, nil
}
func (u *UserRepo) DeleteUser(ctx context.Context, req *mdl.DeleteUserReq) (*mdl.DeleteUserRes, error) {
	return &mdl.DeleteUserRes{}, nil
}

func (u *UserRepo) Exists(ctx context.Context, req *mdl.GetInfoReq) (*mdl.User, error) {
	res := &mdl.User{}
	var updatedAt sql.NullTime
	query := fmt.Sprintf("SELECT id, first_name, last_name, phone_number, birth_date, created_at, updated_at FROM customers WHERE %s = $1", req.Field)
	err := u.db.QueryRowContext(ctx, query, req.Value).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.PhoneNumber,
		&res.BirthDate,
		&res.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	if updatedAt.Valid {
		res.UpdatedAt = updatedAt.Time.Format(time.RFC3339)
	}

	return res, nil
}
