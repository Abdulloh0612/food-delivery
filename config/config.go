package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment      string
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	RedisHost        string
	RedisPort        string
	RedisUsername    string
	RedisPassword    string

	CtxTimeout int

	SigningKey string

	LogLevel string
	HTTPPort string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	c.Environment = cast.ToString(os.Getenv("ENVIRONMENT"))

	c.LogLevel = cast.ToString(os.Getenv("LOG_LEVEL"))
	c.HTTPPort = cast.ToString(os.Getenv("HTTP_PORT"))

	c.PostgresHost = cast.ToString(os.Getenv("POSTGRES_HOST"))
	c.PostgresPort = cast.ToInt(os.Getenv("POSTGRES_PORT"))
	c.PostgresDatabase = cast.ToString(os.Getenv("POSTGRES_DATABASE"))
	c.PostgresUser = cast.ToString(os.Getenv("POSTGRES_USER"))
	c.PostgresPassword = cast.ToString(os.Getenv("POSTGRES_PASSWORD"))

	c.RedisHost = cast.ToString(os.Getenv("REDIS_HOST"))
	c.RedisPort = cast.ToString(os.Getenv("REDIS_PORT"))
	c.RedisUsername = cast.ToString(os.Getenv("REDIS_USERNAME"))
	c.RedisPassword = cast.ToString(os.Getenv("REDIS_PASSWORD"))

	c.CtxTimeout = cast.ToInt(os.Getenv("CTX_TIMEOUT"))
	c.SigningKey = cast.ToString(os.Getenv("SIGNINGKEY"))

	return c
}
