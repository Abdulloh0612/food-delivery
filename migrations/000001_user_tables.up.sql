CREATE TABLE customers (
  id uuid PRIMARY KEY,
  first_name varchar,
  last_name varchar,
  phone_number varchar,
  birth_date varchar,
  refresh_token varchar,
  created_at timestamp DEFAULT (now()),
  updated_at timestamp,
  deleted_at int DEFAULT 0
);

INSERT INTO customers (id, first_name, last_name, phone_number, birth_date)
VALUES
('323e4567-e89b-12d3-a456-426614174002', 'Alice', 'Johnson', '+1112223333', '1990-05-15'),
('423e4567-e89b-12d3-a456-426614174003', 'Bob', 'Williams', '+4445556666', '1985-10-20');

