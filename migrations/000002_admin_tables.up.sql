CREATE TABLE super_admins (
  id uuid PRIMARY KEY,
  user_name varchar,
  password varchar,
  refresh_token varchar(255),
  created_at timestamp DEFAULT (now()),
  updated_at timestamp,
  deleted_at int DEFAULT 0
);

INSERT INTO super_admins (id, user_name, password)
VALUES ('123e4567-e89b-12d3-a456-426614174000', 'abdulloh', '$2a$14$/Sz8yTYLeMFecbqoqOBFLulaIjpZieeWw/dUE.q7s3NrGnap7cBz2');
