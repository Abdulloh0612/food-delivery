package api

import (
	v1 "food-delivery/api/handlers/v1"
	"food-delivery/config"
	"food-delivery/pkg/logger"
	"food-delivery/storage"

	_ "food-delivery/api/docs" // swag

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Option struct {
	Conf     config.Config
	Logger   logger.Logger
	Enforcer *casbin.Enforcer
	Storage  storage.IStorage
}

// New ...
// @title Welcome to Food Delivery
// @version 1.0
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(option Option) *gin.Engine {
	router := gin.Default()
	handlerV1 := v1.New(&v1.HandlerConfig{
		Logger:   option.Logger,
		Storage:  option.Storage,
		Enforcer: option.Enforcer,
		Cfg:      option.Conf,
	})

	api := router.Group("/v1")

	//	Authorization
	auth := api.Group("/auth")
	auth.POST("/authorization", handlerV1.Authorization)
	auth.POST("/login", handlerV1.Login)
	auth.POST("/admin_login", handlerV1.AdminLogin)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
