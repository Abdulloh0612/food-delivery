package models

type Admin struct {
	Id           string `json:"id"`
	UserName     string `json:"first_name"`
	Password     string `json:"password"`
	RefreshToken string `json:"refresh_token"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
}

type AdminLoginReq struct {
	Id           string `json:"-"`
	UserName     string `json:"user_name"`
	Password     string `json:"password"`
	RefreshToken string `json:"-"`
}

type AdminLoginRes struct {
	AccessToken string `json:"access_token"`
}

type GetInfoReq struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type UpdateAdminReq struct {
	Id           string `json:"-"`
	UserName     string `json:"first_name"`
	Password     string `json:"password"`
	RefreshToken string `json:"refresh_token"`
}
