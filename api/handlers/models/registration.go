package models

type RegisterModelReq struct {
	Id          string `json:"-"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
	BirthDate   string `json:"birth_date"`
}

type AuthorizationReq struct {
	PhoneNumber string `json:"phone_number"`
	Code        int    `json:"code"`
}

type UserLoginRes struct {
	AccessToken string `json:"access_token"`
}

type UserLoginReq struct {
	PhoneNumber  string `json:"phone_number"`
	RefreshToken string `json:"-"`
}
