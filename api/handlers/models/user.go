package models

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Id           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	PhoneNumber  string `json:"phone_number"`
	BirthDate    string `json:"birth_date"`
	RefreshToken string `json:"refresh_token"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
	DeletedAt    string `json:"deleted_at"`
}

type CreateUserReq struct {
	Id           string `json:"-"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	PhoneNumber  string `json:"phone_number"`
	BirthDate    string `json:"birth_date"`
	RefreshToken string `json:"-"`
}

type GetUserReq struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type UpdateUserReq struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
	BirthDate   string `json:"birth_date"`
}

type GetAllUsersRes struct {
	Users []User `json:"users"`
}

type GetAllUsersReq struct {
	Limit int64 `json:"limit"`
	Page  int64 `json:"page"`
}

type DeleteUserReq struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type DeleteUserRes struct {
	Status bool `json:"status"`
}

type CheckUniqReq struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type CheckUniqRes struct {
	IsAuthorized bool `json:"is_authorized"`
}
