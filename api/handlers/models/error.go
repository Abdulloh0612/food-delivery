package models

type StandardErrorModel struct {
	StatusCode  int       `json:"Status_code"`
	Description string    `json:"Description"`
	Data        ErrorData `json:"Data"`
}

type ErrorData struct {
	Severity         string `json:"Severity"`
	Code             string `json:"Code"`
	Message          string `json:"Message"`
	Detail           string `json:"Detail"`
	Hint             string `json:"Hint"`
	Position         int    `json:"Position"`
	InternalPosition int    `json:"InternalPosition"`
	InternalQuery    string `json:"InternalQuery"`
	Where            string `json:"Where"`
	SchemaName       string `json:"SchemaName"`
	TableName        string `json:"TableName"`
	ColumnName       string `json:"ColumnName"`
	DataTypeName     string `json:"DataTypeName"`
	ConstraintName   string `json:"ConstraintName"`
	File             string `json:"File"`
	Line             int    `json:"Line"`
	Routine          string `json:"Routine"`
}
