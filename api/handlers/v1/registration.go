package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"food-delivery/api/handlers/models"
	"food-delivery/pkg/etc"
	l "food-delivery/pkg/logger"
	"math/rand/v2"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"github.com/spf13/cast"
)

// Authorization ...
// @Summary Authorization
// @Description Authorization - Api for registering users
// @Tags Authorization
// @Accept json
// @Produce json
// @Param Register body models.AuthorizationReq true "RegisterModelReq"
// @Failure 200 {object} models.UserLoginRes
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/auth/authorization [post]
func (h *HandlerV1) Authorization(c *gin.Context) {
	var body models.AuthorizationReq
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error("Failed to bind json", l.Error(err))
		return
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf(h.cfg.RedisHost + ":" + h.cfg.RedisPort),
		Password: h.cfg.RedisPassword,
	})

	defer func() {
		if err := rdb.Close(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			h.logger.Error(err.Error())
			return
		}
	}()

	_, err = rdb.Ping(ctx).Result()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		h.logger.Error("Failed to connect to Redis", l.Error(err))
		return
	}

	respCode, err := rdb.Get(ctx, body.PhoneNumber).Result()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	var code int
	if err := json.Unmarshal([]byte(respCode), &code); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	code1 := body.Code
	if code != code1 {
		c.JSON(http.StatusBadRequest, false)
		return
	}

	err = rdb.Del(ctx, body.PhoneNumber).Err()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	var regis models.RegisterModelReq
	respUser, err := rdb.Get(ctx, body.PhoneNumber+cast.ToString(code)).Result()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	err = rdb.Del(ctx, body.PhoneNumber+cast.ToString(code)).Err()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	if err := json.Unmarshal([]byte(respUser), &regis); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	user, err := h.storage.User().Exists(ctx, &models.GetInfoReq{Field: "phone_number", Value: body.PhoneNumber})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	if user == nil {
		temp := uuid.New().String()
		access, refresh, err := h.jwthandler.GenerateAuthJWT(temp, "user")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			h.logger.Error(err.Error())
			return
		}
		_, err = h.storage.User().CreateUser(ctx, &models.CreateUserReq{Id: temp, PhoneNumber: body.PhoneNumber, RefreshToken: refresh})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			h.logger.Error(err.Error())
			return
		}
		c.JSON(http.StatusOK, &models.UserLoginRes{
			AccessToken: access,
		})
		return
	}

	access, refresh, err := h.jwthandler.GenerateAuthJWT(user.Id, "user")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	_, err = h.storage.User().Login(ctx, &models.UserLoginReq{PhoneNumber: body.PhoneNumber, RefreshToken: refresh})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	c.JSON(http.StatusOK, &models.UserLoginRes{
		AccessToken: access,
	})

}

// AdminLogin ...
// @Summary AdminLogin
// @Description AdminLogin - Api for registering users
// @Tags Authorization
// @Accept json
// @Produce json
// @Param AdminLogin body models.AdminLoginReq true "Admin Login Req"
// @Success 200 {object} models.AdminLoginRes
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/auth/admin_login [post]
func (h *HandlerV1) AdminLogin(c *gin.Context) {
	var body models.AdminLoginReq
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad request"})
		h.logger.Error(err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	pwd := etc.HashPassword(body.Password)

	user, err := h.storage.Admin().Exists(ctx, &models.GetInfoReq{Field: "user_name", Value: body.UserName})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Admin does not exist"})
		h.logger.Error(err.Error())
		return
	}
	if !etc.CheckPasswordHash(body.Password, pwd) {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Wrong password"})
		return
	}
	var access string
	access, user.RefreshToken, err = h.jwthandler.GenerateAuthJWT(user.Id, "suAdmin")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		h.logger.Error(err.Error())
		return
	}
	_, err = h.storage.Admin().AdminLogin(ctx, &models.AdminLoginReq{
		UserName:     user.UserName,
		Password:     user.Password,
		RefreshToken: user.RefreshToken,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	c.JSON(http.StatusOK, models.AdminLoginRes{AccessToken: access})
}

// Login ...
// @Summary Login
// @Description Login - Api for registering users
// @Tags Authorization
// @Accept json
// @Produce json
// @Param Login body models.UserLoginReq true "Login Req"
// @Success 200 {object} string
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/auth/login [post]
func (h *HandlerV1) Login(c *gin.Context) {
	var body struct {
		PhoneNumber string `json:"phone_number"`
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error("Failed to bind json", l.Error(err))
		return
	}

	fmt.Println("address: ", h.cfg.RedisHost+":"+h.cfg.RedisPort)
	fmt.Println("password: ", h.cfg.RedisPassword)

	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf(h.cfg.RedisHost + ":" + h.cfg.RedisPort),
		Password: h.cfg.RedisPassword,
	})

	_, err = rdb.Ping(ctx).Result()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		h.logger.Error("Failed to connect to Redis", l.Error(err))
		return
	}

	body.PhoneNumber = strings.TrimSpace(body.PhoneNumber)
	body.PhoneNumber = strings.ToLower(body.PhoneNumber)

	code := rand.Int32() % 1000000

	byteDate, err := json.Marshal(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	err = rdb.Set(ctx, body.PhoneNumber, code, time.Second*300).Err()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	err = rdb.Set(ctx, body.PhoneNumber+cast.ToString(code), byteDate, time.Second*300).Err()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	auth := smtp.PlainAuth("test email", "torakhonoffical@gmail.com", "nfjdryidumjowyyv", "smtp.gmail.com")
	err = smtp.SendMail("smtp.gmail.com:587", auth, "torakhonoffical@gmail.com", []string{body.PhoneNumber}, []byte(strconv.Itoa(int(code))))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		h.logger.Error(err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{"Success": "Code sent to email address"})
}
