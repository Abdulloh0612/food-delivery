package v1

import (
	t "food-delivery/api/tokens"
	"food-delivery/config"
	"food-delivery/pkg/logger"
	"food-delivery/storage"

	"github.com/casbin/casbin/v2"
)

type HandlerV1 struct {
	storage    storage.IStorage
	Enforcer   *casbin.Enforcer
	cfg        config.Config
	logger     logger.Logger
	jwthandler t.JWTHandler
}

type HandlerConfig struct {
	Logger     logger.Logger
	Cfg        config.Config
	JWTHandler t.JWTHandler
	Enforcer   *casbin.Enforcer
	Storage    storage.IStorage
}

func New(c *HandlerConfig) *HandlerV1 {
	return &HandlerV1{
		storage:    c.Storage,
		logger:     c.Logger,
		jwthandler: c.JWTHandler,
		cfg:        c.Cfg,
	}
}
